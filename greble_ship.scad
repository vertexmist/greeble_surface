// Greble test
$fn = 20;

SMALLEST_FEATURE = 0.02;

function mixVec3(t, a, b) = [mix(t, a[0], b[0]), 
                              mix(t, a[1], b[1]), 
                              mix(t, a[2], b[2])];
function addVec3(a, b) = [a[0] + b[0], a[1] + b[1], a[2] + b[2]];

function relPosInArea(area, relPos) = [
    relPos[0]*area[0] - area[0]*0.5, 
    relPos[1]*area[1] - area[1]*0.5, 
    relPos[2]*area[2] - area[2]*0.5];
function relPos(t, a, b) = b == a ? 0.5 : (t - a) / (b - a);
function mix(t, a, b) = b * t + a * (1.0 - t);
function map(t, sourceStart, sourceEnd, targetStart, targetEnd) = mix(relPos(t, sourceStart, sourceEnd), targetStart, targetEnd);

function randomInt(maxIntExclusive, seed) = round(rands(0.0, maxIntExclusive, 1, seed)[0]);
function randomReal(maxValue, seed) = rands(0.0, maxValue, 1, seed)[0];

function hash(a, b = 13, c = 7) = (a * 31.13 + b * 19.71) * 2.7 + c * 7.8;


function distance(a, b) = sqrt( (a[0] - b[0]) * (a[0] - b[0]) + 
                                (a[1] - b[1]) * (a[1] - b[1]) + 
                                (a[2] - b[2]) * (a[2] - b[2]) );

function lengthVec2(a) = sqrt( a[0]*a[0] + a[1]*a[1] );
function lengthVec3(a) = sqrt( a[0]*a[0] + a[1]*a[1] + a[2]*a[2] );

function normalizedVec3(a) = let(l = lengthVec3(a)) l <= 0 ? [0,0,1] : [a[0]/l, a[1]/l, a[2]/l];


function angleOfNormalizedVector(n) = [0, -atan2(n[2], lengthVec2([n[0], n[1]])), atan2(n[1], n[0]) ];

function angle(v) = angleOfNormalizedVector(normalizedVec3(v));

function angleBetweenTwoPoints(a, b) = angle(normalizedVec3(b-a));

module connect(a=[0,0,0], b=[1,1,1], size=[1, 1], align=[0.5, 0.5], endExtras=[0,0], rotation=[0,0,0]) {

  angle = angleBetweenTwoPoints(a, b);
  length = distance(a, b) + endExtras[0] + endExtras[1];

  if (length > 0) {
      translate(a)
        rotate(angle) 
          translate( [- endExtras[0], size[0]*(-0.5-align[0]), size[1]*(-0.5+align[1]) ] )
            rotate(rotation) 
              scale([length, size[0], size[1]]) children();
  }
}

function randomMargin(a, b, seed) = let(type = randomReal(100, seed)) 
    type <= 1 ? b * 0.25  :
    type <= 2 ? a * 0.25  :
    type <= 10 ?  b * 0.15 :
    type <= 20 ?  a * 0.15 :
    type <= 40 ?  b * 0.05 :
    type <= 60 ?  a * 0.05 :
    0;

module sphereGreeble(size, seed) {
    r = min(size.x, size.y) / 2.0;
    offs = (randomInt(4, seed) * 0.25 - 0.8)* r;
    translate([0,0,offs])
        sphere(r);
}

module cylinderGreeble(size, seed) {
    flip = size.y > 2*size.x;
    long = flip ? size.y : size.x;
    short = flip ? size.x : size.y;
    r = short * 0.1 * (1+randomInt(3, seed));
    h = long * 0.125 * (randomInt(4, seed));
    rotate([0,90,flip ? 90 : 0]) {
        translate([0, 0, -h*0.5]) sphere(r);
        cylinder(h, r=r, center=true);
        translate([0, 0, h*0.5]) sphere(r);
    }
}

module coolerRing(r1, r2, h, num, width, thickness) {
    flooring = width*2;
    dr = r2-r1;
    if (num > 0) {
        intersection() {
            mr = max(r1, r2);
            cube([mr*3, mr*3, h*2], center=true);
            for (i = [1:num]) {
                rotate([0,0,i*360/num])
                    translate([r1, -width/2, 0])
                        rotate([0, dr == 0 ? 0 : atan(dr/h), 0])
                            translate([0,0,-flooring])
                                cube([thickness, width, flooring+sqrt(h*h+dr*dr)]);
            }
        }
    }
}

module randomCone(maxR, maxH, seed, cooler=true) {
    h = maxH * 0.1 * (1+randomInt(4, seed+1));
    r1 = maxR * (0.7 + 0.1 * randomInt(4, seed+ 3));
    r2 = maxR * (0.4 + 0.1 * randomInt(4, seed + 5));
    cylinder(h, r1, r2);
    if (cooler) {       
        coolerRing(r1, r2, h, 3*randomInt(4, seed+9.9), h*0.1*randomInt(4, seed+13), h*0.08*randomInt(4, seed+7));
    }
}

module coneGreeble(size, seed) {
    r = min(size.x, size.y) * 0.5;
    h = size.z / 2;
    
    difference() {
        randomCone(r, h, seed+17.2, true);
        translate([0,0,h*1.01])
            mirror([0,0,1]) {
                randomCone(r*0.5, h*4, seed+48.2, false);
                randomCone(r*0.9, h*1.5, seed+27.2, false);
            }
    }
}

module boxGreeble(size_, seed) {
    flip = size_.y > size_.x * 1.2;
    size = flip ? [size_.y, size_.x, size_.z] : size_;
    rot = flip ? 90 : 0;

    rotate([0,0,rot]) {
        minXY = min(size.x, size.y);   
        thickness = min(minXY, size.z) ;
        
        t = randomMargin(thickness, size.z, seed + 2) * 1. + 0.25 * size.z;

        marginX = size.x*0.1;
        marginY = size.y*0.1;
        boxSize = [size.x - marginX*2, size.y - marginY*2, t];
       
        bb = marginX*0.4;
        translate([-size.x*0.5+marginX, -size.y*0.5+marginY, 0]) {
            cube(boxSize);
            translate([bb, bb, boxSize.z-0.1*bb])
                cube([boxSize.x-bb*2, boxSize.y-bb*2, bb]);
            
        }
        
        // Supports
        xw = boxSize.x * 0.04 * randomInt(4, seed+17);
        if (randomReal(1, seed+5) < 0.75 && xw > SMALLEST_FEATURE) {
            supportCount = randomInt(6, seed+7) + 1;
            x1 = mix(randomReal(0.3, seed+13), -boxSize.x*0.5, boxSize.x*0.5);
            x2 = mix(randomReal(0.3, seed+17)+0.7, -boxSize.x*0.5, boxSize.x*0.5);
            thickness = t * (0.1 + 0.1 * randomInt(4, seed+21.7));
            for (i = [1:supportCount]) {
                x = map(i, 1, supportCount, x1, x2) - xw*0.5;
                translate([x, -boxSize.y*0.5-thickness/2, 0])
                    cube([xw, boxSize.y+thickness, boxSize.z+thickness/2]);
                st = xw*0.4;
                translate([x-st, -boxSize.y*0.5-thickness/2-st, 0])
                    cube([xw+st*2, boxSize.y+thickness+st*2, st*4]);
            }
        }
    }
    
}

module slopeGreeble(size, seed) {
    minXY = min(size.x, size.y);   
    thickness = min(minXY, size.z) ;
   
    t = randomMargin(thickness, size.z, seed + 2) * 1. + 0.05 * size.z;
    translate([-size.x*0.5, -size.y*0.5, 0]) {
        type = randomInt(3, seed+1);
        slope = 15 * randomInt(5, seed+2); 
        intersection() {
            rotate([slope, 0, 0]) {
                et = size.z;
                translate([0,0,-et])
                    difference() {
                        cube([size.x, size.y, t + et]);
                        depth = 1.0 - 0.05 * randomInt(3, seed+18);
                        translate([size.x*0.1,size.y*0.1,size.z*depth])
                            cube([size.x*0.8, size.y*0.8, t + et]);
                    }
            }
            cube(size);
        }
    }
}

function prevGridStep(i, grid) = floor(i / grid) * grid;

// Check if we should generate a larger sub-panel here
function panelSize(x, y, maxX, maxY, seed, scale = 2, largePanelProb = 0.35)  = (x < 0 || y < 0 || x >= prevGridStep(maxX, scale) || y >= prevGridStep(maxY, scale)) ? 1 :randomReal(1.0, hash(floor(x / scale), floor(y / scale), seed+8.39 + scale)) < largePanelProb ? ((x % scale) == 0 && (y % scale) == 0? scale : 0) : 1;

function mergePanelSizes(a, b) = a == 0 || b == 0 ? 0 : max(a,b);
function comboPanelSize(x, y, maxX, maxY, seed, scale1 =2, scale2 = 4, prob1 = 0.25, prob2 = 0.25) = mergePanelSizes(panelSize(x, y, maxX, maxY, seed+3.12, scale1, prob1), panelSize(x, y, maxX, maxY, seed+17.31, scale2, prob2));


module pipe(seed, start, mid, end, size) {
    connect(start, mid, rotation=[0,0,0]) {        
        cylinder(r = size, h = 1);    
    }
    connect(mid, end, rotation=[0,0,0]) {        
        cylinder(r = size, h = 1);    
    }
}

module pipeLine(seed, pipeStart, pipeEnd, pipeHeight, pipeSize, terminateStart = true, terminateEnd = true, recurseLevels = 3) {
//    if (recurseLevels <= 0) {
        mid = addVec3(mixVec3(0.5, pipeStart, pipeEnd), [0,0,pipeHeight]);
//        pipe(seed, pipeStart, mid, pipeEnd, pipeSize);    

    connect(pipeStart, pipeEnd, rotation=[0,0,0]) {        
        cube();    
    }

    /*
    }
    else {
        bends = 4;
        for (i = [1:bends]) {
            
        }
    }
    */
}

module gridPipes(seed, size, xGrids, yGrids, relPipeHeight = 0.5) {
    // Use manhattan distance finder, with some detours?
    num = 1;
    for (i = [1:num]) {
        rsx = randomInt(xGrids, seed+2) / xGrids;
        rsy = randomInt(yGrids, seed+6) / yGrids;
        rex = randomInt(xGrids, seed+8) / xGrids;
        rey = randomInt(yGrids, seed+11) / yGrids;
        start = relPosInArea(size, [rsx, rsy, 0.5]);
        end = relPosInArea(size, [rex, rey, 0.5]);
        
        //pipeLine(seed, start, end, size[2]*relPipeHeight);
        pipe(seed, start, end, size[2]*relPipeHeight);
    }
}

module subPanels(size, seed, maxRecursionsLeft = 3, num = 2, additional = 1, extraBaseThickness = 0, sameSeed = false, ratio = 0.5) {
    
    // Base
    thicknessZ = size.z * 0.3 * randomMargin(1, 0, seed+3) + extraBaseThickness * size.z;
    translate([-size.x*0.5, -size.y*0.5, 0]) cube([size.x, size.y, thicknessZ]);
    
    // Divide into a grid, and fill with greebles
    xCells = max(1, round((randomReal(num, seed+1) + additional) * 2 * ratio));
    yCells = max(1, round((randomReal(num, seed+2) + additional) * 2 * (1-ratio)));
    xSize = size.x / xCells;
    ySize = size.y / yCells;
    zSize = size.z * 0.5;
    
    commonSeed = hash(seed+13);
    
    for (yCell = [0:yCells-1]) {
        for (xCell = [0:xCells-1]) {
            cellSeed = sameSeed ? commonSeed : hash(seed, yCell, xCell);
            x = xCell * xSize - size.x * 0.5 + xSize*0.5;
            y = yCell * ySize - size.y * 0.5 + ySize*0.5;
            z = thicknessZ;
            panelSizeX = comboPanelSize(xCell, yCell, xCells, yCells, seed + 5.896);
            panelSizeY = panelSizeX;
            if (panelSizeX > 0 && panelSizeY > 0) {
                translate([x + (xSize) * ((panelSizeX-1.)/2.0), 
                          y + (ySize) * ((panelSizeY-1.)/2.0), 
                          z]) {
                   greeble([xSize*panelSizeX, ySize*panelSizeY, zSize], cellSeed, maxRecursionsLeft - 1);
                }
            }
        }
    }    

    // Pipes
 //   gridPipes(seed, size, xCells, yCells);
}

module margin(size, seed) {
    minXY = min(size.x, size.y);   
    marginX = randomMargin(minXY, size.x, seed + 1);
    marginY = randomMargin(minXY, size.y, seed + 2);
    scaleX = 1.0 - 2.0 * marginX / size.x;
    scaleY = 1.0 - 2.0 * marginY / size.y;
    translate([-size.x*0.5+marginX, -size.y*0.5+marginY, 0]) {
        scale([scaleX, scaleY, 1.0]) children();
    }    
}

module boxFrame(size, seed) {
    sideScale = 0.05 * randomInt(3, seed+2.8);
    heightScale = 0.05 * randomInt(2, seed+4.7);

    translate([-size.x/2,-size.y/2,0]) 
        cube([size.x*sideScale, size.y, size.z * heightScale]);
    translate([size.x/2-sideScale*size.x, -size.y/2, 0]) 
        cube([size.x*sideScale, size.y, size.z * heightScale]);

    translate([-size.x/2,-size.y/2,0]) 
        cube([size.x, size.y*sideScale, size.z * heightScale]);
    translate([-size.x/2, size.y/2-sideScale*size.y, 0]) 
        cube([size.x, size.y*sideScale, size.z * heightScale]);
}

module plusFrame(size, seed) {
    sideScale = 0.05 * randomInt(3, seed+2.8);
    heightScale = 0.05 * randomInt(2, seed+4.7);
    translate([-sideScale*size.x*0.5, -size.y/2, 0]) 
        cube([size.x*sideScale, size.y, size.z * heightScale]);

    translate([-size.x/2, -sideScale*size.y*0.5, 0]) 
        cube([size.x, size.y*sideScale, size.z * heightScale]);
}

module beamFrame(size, seed) {
    sideScale = 0.05 * randomInt(3, seed+2.8);
    heightScale = 0.05 * randomInt(2, seed+4.7);
    translate([-sideScale*size.x*0.5, -size.y/2, 0]) 
        cube([size.x*sideScale, size.y, size.z * heightScale]);

}

module tiltBeamFrame(size, seed) {
    sideScale = 0.05 * randomInt(3, seed+2.8);
    heightScale = 0.05 * randomInt(2, seed+4.7);
    rot = randomInt(12, seed+13.3) * 15;
    xOffs = size.x * (randomInt(6, seed+31.3) / 6 - 0.5);
    yOffs = size.y * (randomInt(6, seed+24.31) / 6 - 0.5);
    
    intersection() {
        translate([-size.x/2, -size.y/2, 0]) cube(size);
        translate([xOffs, yOffs, 0])
            rotate([0, 0, rot])
                translate([-size.x*sideScale*0.5, -size.y*.5, size.z*heightScale*0.5])
                    cube([size.x*sideScale, size.y*3, size.z * heightScale], center=true);
    }
    
    translate([-size.x/2,-size.y/2,0]) 
        cube([size.x*sideScale, size.y, size.z * heightScale]);
    translate([size.x/2-sideScale*size.x, -size.y/2, 0]) 
        cube([size.x*sideScale, size.y, size.z * heightScale]);

    translate([-size.x/2,-size.y/2,0]) 
        cube([size.x, size.y*sideScale, size.z * heightScale]);
    translate([-size.x/2, size.y/2-sideScale*size.y, 0]) 
        cube([size.x, size.y*sideScale, size.z * heightScale]);

}

module greebleFrame(size, seed, maxRecursionsLeft = 4) {
    frameRot = randomInt(4, hash(seed+11.17));
    frameRotationZ = frameRot * 90;
    frameSwapXYSize = frameRot == 1 || frameRot == 3;

    fsx = size.x;
    fsy = size.y;
    frameSize = [frameSwapXYSize ? fsy : fsx,
                   frameSwapXYSize ? fsx : fsy,
                   size.z*0.5];
    
    rotate([0,0,frameRotationZ]) {                
        f = randomReal(100, hash(seed+3));
        if (f <= 30) boxFrame(frameSize, seed);
        else if (f <= 60) tiltBeamFrame(frameSize, seed);
        else if (f <= 70) plusFrame(frameSize, seed);
        else if (f <= 80) beamFrame(frameSize, seed);
        // Else nothing
    }
        
}


module greeble(size, seed, maxRecursionsLeft = 4) {
    if (maxRecursionsLeft < 0) cube([0,0,0]);
    else {
        
        minXY = min(size.x, size.y);   
        marginX = randomMargin(minXY, size.x, hash(seed + 1));
        marginY = randomMargin(minXY, size.y, hash(seed + 2));

        rot = randomInt(4, hash(seed+4));
        rotationZ = rot * 90;
        swapXYSize = rot == 1 || rot == 3;

        sx = size.x - marginX*2;
        sy = size.y - marginY*2;
        greebleSize = [swapXYSize ? sy : sx,
                       swapXYSize ? sx : sy,
                       size.z];
        

        g = randomReal(100, hash(seed+3));
        rotate([0,0,rotationZ]) {                
            if (g <= 10) boxGreeble(greebleSize, seed);
            else if (g <= 15) sphereGreeble(greebleSize, seed);
            else if (g <= 25) cylinderGreeble(greebleSize, seed);
            else if (g <= 40) coneGreeble(greebleSize, seed);
            else if (g <= 50) slopeGreeble(greebleSize, seed);
            else if (g <= 75) subPanels(greebleSize, seed, maxRecursionsLeft-1);
            else if (g <= 88) subPanels(greebleSize, seed, maxRecursionsLeft-1, num=2, additional=1,sameSeed=true);
            else if (g <= 92) subPanels(greebleSize, seed, maxRecursionsLeft-1, num=4, additional=2,sameSeed=true);
            else if (g <= 97) subPanels(greebleSize, seed, maxRecursionsLeft-1, num=3, additional=2,sameSeed=true, ratio = randomInt(2, seed+7));
            else boxGreeble(greebleSize, seed);
        }

        // Frame
        if (maxRecursionsLeft > 2) {
            greebleFrame(size, seed+3.98, maxRecursionsLeft);
        }
    }    
}

    // Ideas:
    // * Larger panels
    // * lines or grids with same seed
    
    
module greebledSurface(size, seed = 1.73, maxRecursions = 4,
 num = 5, additionalNum = 8) {

     intersection() {
         subPanels([size.x,size.y,size.z*2], seed, maxRecursions, num, additionalNum, 0.03);
         translate([-size.x*0.5, -size.y*0.5, 0]) cube(size); 
     }

       
}


greebledSurface([30,30,10], -9.12, 4, 6, 6);


